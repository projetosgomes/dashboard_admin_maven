<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<footer class="site-footer">
	<div class="footer-inner bg-white">
		<div class="row">
			<div class="col-sm-6">Copyright &copy; 2019 Gomes</div>
			<div class="col-sm-6 text-right">Designed by Paulo Gomes
				Marques</div>
		</div>
	</div>
</footer>
