<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Barbearias ou Salões de beleza -->
<div class="card-title">
	<h4>Barbearias ou Salões de beleza</h4>
</div>
<div class="card-title">
	<h5>Dados do responsavél pelo estabelecimento:</h5>
</div>
<div class="row  form-group">
	<div class="input-group col-md-6" >
		<div class="input-group-addon">CPF</div>
		<input type="text" id="documentoCPF" name="documentoCPF" class="form-control cpf" placeholder="Ex: 000.000.000-00" autocomplete="false" maxlength="14">
		<div class="input-group-addon">
			<i class="fa fa-user"></i>
		</div>
	</div>
	<div class="input-group col-md-6">
		<div class="input-group-addon">Nome</div>
		<input type="text" id="nomeCompleto" name="nomeCompleto" class="form-control" placeholder="Ex: João da Silva" autocomplete="false" >
		<div class="input-group-addon">
			<i class="fa fa-user"></i>
		</div>
	</div>
</div>
<hr>
<div class="card-title">
	<h5>Informações do estabelecimento:</h5>
</div>
<div class="form-group">
	<div class="input-group">
		<div class="input-group-addon">CNPJ</div>
		<input type="text" id="documentoCNPJ" name="documentoCNPJ" class="form-control" placeholder="Ex: 000.000.000/0000-00" autocomplete="false">
		<div class="input-group-addon">
			<i class="fa fa-building-o"></i>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="input-group">
		<div class="input-group-addon">Razão Social</div>
		<input type="text" id="razaoSocial" name="razaoSocial" class="form-control" placeholder="Ex: Empresa exemplo" autocomplete="false" maxlength="60" >
		<div class="input-group-addon">
			<i class="fa fa-building-o"></i>
		</div>
	</div>
</div>
<div class="form-group">
    <div class="input-group">
        <div class="input-group-addon">E-mail</div>
        <input type="email" id="emailComercial" name="emailComercial" class="form-control" placeholder="exemplo@exemplo.com.br" autocomplete="false">
        <div class="input-group-addon"><i class="fa fa-building-o"></i></div>
    </div>
</div>
<div class="form-group">
    <div class="input-group">
        <div class="input-group-addon">Celular</div>
        <input type="text" id="celularComercial" name="celularComercial" class="form-control" placeholder="(00) 0-0000-0000" autocomplete="false">
        <div class="input-group-addon"><i class="fa fa-building-o"></i></div>
    </div>
</div>
<hr>
<div class="card-title">
	<h5>Dados de acesso a aplicação:</h5>
</div>
<div class="form-group">
	<div class="input-group">
        <div class="input-group-addon">Login</div>
        <input type="text" id="cc-login" name="cc-login" class="form-control" aria-required="true" aria-invalid="false" disabled autocomplete="false">
        <div class="input-group-addon"><i class="fa fa-building-o"></i></div>
    </div>	
</div>
<div class="form-group">
	<div class="input-group">
        <div class="input-group-addon">Senha</div>
        <input id="password" name="password" type="password" class="form-control cc-password valid" aria-required="true" aria-invalid="false" aria-describedby="password"  autocomplete="false">
        <div class="input-group-addon"><i class="fa fa-building-o"></i></div>
        <span class="help-block field-validation-valid" data-valmsg-for="cc-password" data-valmsg-replace="true"></span>
    </div>
</div>
<div class="form-group">
	<div class="input-group">
        <div class="input-group-addon">Confirmação da senha</div>
        <input id="c-password" name="c-password" type="password" class="form-control c-password valid" aria-required="true" aria-invalid="false" aria-describedby="c-password"  autocomplete="false">
        <div class="input-group-addon"><i class="fa fa-building-o"></i></div>
        <span class="help-block field-validation-valid" data-valmsg-for="cc-password" data-valmsg-replace="true"></span>
    </div>
</div>
<c:import url="button-save.jsp" />