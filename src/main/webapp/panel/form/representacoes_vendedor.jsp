<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Representante -->
<div class="card-title">
	<h4>Representante de Vendas</h4>
</div>
<div class="card-title">
	<h5>Dados do solicitante:</h5>
</div>
<div class="form-group">
	<div class="input-group">
		<div class="input-group-addon">CPF</div>
		<input type="text" id="documentoCPF" name="documentoCPF" class="form-control" placeholder="Ex: 000.000.000-00">
		<div class="input-group-addon">
			<i class="fa fa-user"></i>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="input-group">
		<div class="input-group-addon">Nome</div>
		<input type="text" id="nomeCompleto" name="nomeCompleto" class="form-control" placeholder="Ex: João da Silva">
		<div class="input-group-addon">
			<i class="fa fa-user"></i>
		</div>
	</div>
</div>
<hr>
<div class="card-title">
	<h5>Informações da empresa representadora:</h5>
</div>
<div class="form-group">
	<div class="input-group">
		<div class="input-group-addon">CNPJ</div>
		<input type="text" id="documentoCNPJ" name="documentoCNPJ" class="form-control" placeholder="Ex: 000.000.000/0000-00">
		<div class="input-group-addon">
			<i class="fa fa-building-o"></i>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="input-group">
		<div class="input-group-addon">Razão Social</div>
		<input type="text" id="razaoSocial" name="razaoSocial" class="form-control" placeholder="Ex: Empresa exemplo">
		<div class="input-group-addon">
			<i class="fa fa-building-o"></i>
		</div>
	</div>
</div>
<div class="card-title">
	<h5>Contato comercial:</h5>
	<p>Dados de contato para uso comercial.</p>
</div>
<div class="form-group">
    <div class="input-group">
        <div class="input-group-addon">E-mail</div>
        <input type="email" id="emailComercial" name="emailComercial" class="form-control" placeholder="exemplo@exemplo.com.br">
        <div class="input-group-addon"><i class="fa fa-building-o"></i></div>
    </div>
</div>
<c:import url="button-save.jsp"/>