<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Representante -->
<div class="card-title">
	<h4>Desmontes de Veículos</h4>
</div>
<div class="card-title">
	<h5>Dados da empresa:</h5>
</div>
<div class="form-group">
	<div class="input-group">
		<div class="input-group-addon">Razão Social:</div>
		<input type="text" id="completname" name="completname"
			class="form-control" placeholder="Ex: Empresa exemplo LTDA">
		<div class="input-group-addon">
			<i class="fa fa-user"></i>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="input-group">
		<div class="input-group-addon">CNPJ</div>
		<input type="text" id="documentCPF" name="documentCPF"
			class="form-control" placeholder="Ex: 000.000.000/0000-00">
		<div class="input-group-addon">
			<i class="fa fa-user"></i>
		</div>
	</div>
</div>
<div class="form-group">
    <div class="input-group">
        <div class="input-group-addon">E-mail</div>
        <input type="email" id="email" name="email" class="form-control" placeholder="exemplo@exemplo.com.br">
        <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
    </div>
</div>
<hr>
<div class="card-title">
	<h5>Dados do solicitante:</h5>
</div>
<div class="form-group">
	<div class="input-group">
		<div class="input-group-addon">Nome</div>
		<input type="text" id="completname" name="completname"
			class="form-control" placeholder="Ex: João da Silva">
		<div class="input-group-addon">
			<i class="fa fa-user"></i>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="input-group">
		<div class="input-group-addon">CPF</div>
		<input type="text" id="documentCPF" name="documentCPF"
			class="form-control" placeholder="Ex: 000.000.000-00">
		<div class="input-group-addon">
			<i class="fa fa-user"></i>
		</div>
	</div>
</div>
<div class="form-group">
    <div class="input-group">
        <div class="input-group-addon">E-mail</div>
        <input type="email" id="email" name="email" class="form-control" placeholder="exemplo@exemplo.com.br">
        <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
    </div>
</div>
