<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="card-body" style="display:none">
	<div class="alert alert-primary" role="alert" style="display:none"></div>
	<div class="alert alert-danger" role="alert" style="display:none"></div>
	<div class="alert alert-warning" role="alert" style="display:none"></div>
</div>