<%@ page contentType="text/html; charset=UTF-8"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Gomes Soluções - Login</title>
<meta name="description" content="Ela Admin - HTML5 Admin Template">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="apple-touch-icon" href="images/favicon.png">
<link rel="shortcut icon" href="images/favicon.png">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
<link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
<link rel="stylesheet" href="assets/css/style.css">
<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
<link
	href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css"
	rel="stylesheet">
<link
	href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css"
	rel="stylesheet">

<link
	href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css"
	rel="stylesheet" />
<link
	href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css"
	rel="stylesheet" />

<c:import url="panel/css/content-clear.css"/>
</head>

<body>

	<!-- Right Panel -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<header id="header" class="header">
			<!-- top-left -->
			<c:import url="panel/top-left.jsp" />
			<!-- ./site - top-left -->
		</header>
		<!-- /#header -->
		<!-- Content -->
		<div class="content">
			<!-- Animated -->
			<div class="animated fadeIn">
				<div class="row align-items-center justify-content-center">
					<div class="col-11 col-md-8 col-lg-4">
						<div class="card">
							<div class="card-header">
								<strong class="card-title">Login</strong>
							</div>
							<div class="card-body">
								<!-- Login -->
								<div id="pay-invoice">
									<div class="card-body">
										<form action="#" method="post">
											<div class="form-group">
												<label for="cc-login" class="control-label mb-1">Usuário</label>
												<input id="cc-login" name="cc-login" type="text" class="form-control" aria-required="true" aria-invalid="false">
											</div>
											<div class="form-group has-success">
												<label for="cc-password" class="control-label mb-1">Senha</label>
												<input id="cc-password" name="cc-password" type="password" class="form-control cc-password valid" aria-required="true" aria-invalid="false" aria-describedby="cc-password"  autocomplete="false">
												<span class="help-block field-validation-valid" data-valmsg-for="cc-password" data-valmsg-replace="true"></span>
											</div>
											<div class="form-group">
												<button id="login-button" type="submit" class="btn btn-lg btn-info btn-block">
													<i class="fa fa-lock fa-lg"></i>&nbsp;
														<span id="login-button-amount">Entrar</span>
												</button>
											</div>
											<div class="form-group">
												<button id="register-button" type="button" class="btn btn-lg btn-info btn-block">
													<i class="fa fa-edit fa-lg"></i>&nbsp; 
													<span id="login-button-amount">Inscrever-se</span>
												</button>
												<script type="text/javascript">
												    document.getElementById("register-button").onclick = function () {location.href = "registrar.jsp";}
												</script>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
						<!-- .Login -->
					</div>
					<!--/.col-->
				</div>
				<!-- /#right-panel -->				
			</div>
			<!-- .animated -->
		</div>
		<!-- /.content -->
	</div>
	<!-- /.Right Panel -->
	<!-- Footer -->
	<c:import url="panel/footer.jsp" />
	<!-- /.site-footer --
    </div>
    <!-- /#right-panel -->

	<!-- Scripts -->
	<script
		src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
	<script src="assets/js/main.js"></script>

	<!--  Chart js -->
	<script
		src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

	<!--Chartist Chart-->
	<script
		src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

	<script
		src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>

	<script
		src="https://cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script>
	<script src="assets/js/init/weather-init.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
	<script src="assets/js/init/fullcalendar-init.js"></script>
</body>
</html>
