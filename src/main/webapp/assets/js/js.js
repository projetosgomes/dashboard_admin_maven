$(function() {	
	
	$("#documentoCPF").change(function(){
		$("#cc-login").val($("#documentoCPF").val().replace(/[^\d]+/g,''));
		
	});
	
	$("#documentoCPF").mask('000.000.000-00').attr('maxlength','14');
	$("#nomeCompleto").attr('maxlength','60');	
	$("#documentoCNPJ").mask('000.000.000/0000-00').attr('maxlength','19');
	$("#razaoSocial").attr('maxlength','60');
	$("#emailComercial").attr('maxlength','40');	
	$("#celularComercial").mask('(00) 00000-0000').attr('maxlength','19');
	
	
	
	function messageAjax(data,jqXHR, textStatus, msg,tipoAlerta){
		$('.alert-primary').css("display","none");
		$('.alert-warning').css("display","none");
		$('.alert-danger').css("display","none");
		if((jqXHR != null || textStatus != null|| msg != null) && data == null){
			var msg = '';
	        if (jqXHR.status === 0) {
	            msg = 'Conexão não estabelecida.\n Verifique seu acesso a internet';
	        } else if (jqXHR.status == 404) {
	            msg = 'Page not found. [404]';
	        } else if (jqXHR.status == 500) {
	            msg = 'Internal Server Error [500].';
	        } else if (exception === 'parsererror') {
	            msg = 'Requested JSON parse failed.';
	        } else if (exception === 'timeout') {
	            msg = 'Time out error.';
	        } else if (exception === 'abort') {
	            msg = 'Ajax request aborted.';
	        } else {
	            msg = 'Uncaught Error.\n' + jqXHR.responseText;
	        }
			$('.alert-danger').css("display","block").append("Erro : "+msg);
			$('.card-body').css("display","block");
			$('.load').removeClass('load').addClass('load-closed');
			
		}else if(data != null && tipoAlerta == 1 ){
			$('.alert-primary').css("display","block").append(data);
			$('.card-body').css("display","block");
			$('.load').removeClass('load').addClass('load-closed');	
		}else if(data != null && tipoAlerta == 2 ){
			$('.alert-warning').css("display","block").append(data);
			$('.card-body').css("display","block");
			$('.load').removeClass('load').addClass('load-closed');
		}else if(data != null && tipoAlerta == 3 ){
			$('.alert-danger').css("display","block").append(data);
			$('.card-body').css("display","block");
			$('.load').removeClass('load').addClass('load-closed');
		}else if(jqXHR == null && textStatus == null && msg == null && data == null){
			$('.load-closed').removeClass('load-closed').addClass('load');
		}
		
	}
	
	// -- Ao clicar no botão inscrever-se com a seleção de representante de vendas
	// -- O codigo sera executado
	$('#save-register-button').click(function(event) {
		if(true){
			$.ajax({
				url : "signup",
				type : 'post',
				data : {
					tipoFinalidade : $("#tipoFinalidade" ).val(),
					action: 'Signup',
					documentoCPF : $('#documentoCPF').val().replace(/[^\d]+/g,''),
					nomeCompleto : $('#nomeCompleto').val(),
					documentoCNPJ : $("#documentoCNPJ").val().replace(/[^\d]+/g,''),
					razaoSocial : $("#razaoSocial").val(),
					emailComercial : $("#emailComercial").val(),
					celularComercial: $("#celularComercial").val().replace(/[^\d]+/g,''),	
				},
				beforeSend : function() {
					messageAjax(null,null,null,null,0);
				},
			}).done(function(data) {
				messageAjax(data,null,null,null,1);					
			}).fail(function(jqXHR, textStatus, msg) {
				messageAjax(null,jqXHR, textStatus, msg,3);
			});
		}else{
			var data = "<strong>Atenção!</strong> Favor preencher/corrigir os campos demarcados.";
			messageAjax(data,null,null,null,2)
		}
	});
	
});