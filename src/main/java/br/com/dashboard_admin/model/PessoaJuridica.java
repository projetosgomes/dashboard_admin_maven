package br.com.dashboard_admin.model;

public class PessoaJuridica extends Pessoa {
	
	private int idPessoaJuridica;
	private String numeroCNPJ;
	private String razaoSocial;
	private String emailEmpresa;
	private String numeroCelular;
	private String tipoEmpresa;
	public PessoaJuridica() {
		super();
	}
	public String getEmailEmpresa() {
		return emailEmpresa;
	}
	
	public int getIdPessoaJuridica() {
		return idPessoaJuridica;
	}
	public String getNumeroCelular() {
		return numeroCelular;
	}
	public String getNumeroCNPJ() {
		return numeroCNPJ;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public String getTipoEmpresa() {
		return tipoEmpresa;
	}
	public void setEmailEmpresa(String emailEmpresa) {
		this.emailEmpresa = emailEmpresa;
	}
	public void setIdPessoaJuridica(int idPessoaJuridica) {
		this.idPessoaJuridica = idPessoaJuridica;
	}
	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}
	public void setNumeroCNPJ(String numeroCNPJ) {
		this.numeroCNPJ = numeroCNPJ;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setTipoEmpresa(String tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}

}
