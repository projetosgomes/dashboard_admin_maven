package br.com.dashboard_admin.model;

public class PessoaFisica extends Pessoa {
	
	private int idPessoaFisica;
	private String nomeCompleto;
	private String numeroCPF;

	public PessoaFisica() {
		super();
	}
	public int getIdPessoaFisica() {
		return idPessoaFisica;
	}
	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public String getNumeroCPF() {
		return numeroCPF;
	}

	public void setIdPessoaFisica(int idPessoaFisica) {
		this.idPessoaFisica = idPessoaFisica;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public void setNumeroCPF(String numeroCPF) {
		this.numeroCPF = numeroCPF;
	}

}
