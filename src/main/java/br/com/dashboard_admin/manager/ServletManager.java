package br.com.dashboard_admin.manager;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dashboard_admin.interfaces.ActionServlet;


@SuppressWarnings("serial")
@WebServlet(urlPatterns = { "/signup" })
public class ServletManager extends HttpServlet {
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			
			Class<?> classe = Class.forName("br.com.dashboard_admin.controller." + req.getParameter("action")+"Controller");
			ActionServlet action = (ActionServlet) classe.newInstance();
			resp.getWriter().print(action.executeAction(req, resp));
		} catch (Exception e) {
			throw new ServletException("O correu um erro ao executar a transação.", e);
		}
	}

}
