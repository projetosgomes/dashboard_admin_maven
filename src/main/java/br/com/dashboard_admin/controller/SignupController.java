package br.com.dashboard_admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dashboard_admin.interfaces.ActionServlet;
import br.com.dashboard_admin.model.PessoaFisica;
import br.com.dashboard_admin.model.PessoaJuridica;

public class SignupController implements ActionServlet {

	@Override
	public String executeAction(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		PessoaFisica pessoaFisica = new PessoaFisica();
		PessoaJuridica pessoaJuridica = new PessoaJuridica();
		
		
		pessoaFisica.setNomeCompleto((String) req.getParameter("nomeCompleto"));
		pessoaFisica.setNumeroCPF((String) req.getParameter("documentoCPF"));
		
		pessoaJuridica.setTipoEmpresa((String) req.getParameter("tipoFinalidade"));
		pessoaJuridica.setRazaoSocial((String) req.getParameter("razaoSocial"));
		pessoaJuridica.setNumeroCNPJ((String) req.getParameter("documentoCNPJ"));
		pessoaJuridica.setNumeroCelular((String) req.getParameter("celularComercial"));
		pessoaJuridica.setEmailEmpresa((String) req.getParameter("emailComercial"));
		pessoaJuridica.setTipoEmpresa((String) req.getParameter("tipoFinalidade"));
		
		return pessoaJuridica.getTipoEmpresa()+pessoaFisica.getNumeroCPF();
	}

}
