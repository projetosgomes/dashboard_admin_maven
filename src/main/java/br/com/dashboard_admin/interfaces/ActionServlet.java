package br.com.dashboard_admin.interfaces;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ActionServlet  {
	
	String executeAction(HttpServletRequest req,	HttpServletResponse res)
			throws	Exception;

}
